import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('http://intelycare.com/hub')

WebUI.setText(findTestObject('359_OR/Page_IntelyCare Login/input_email'), 'skulkarni@intelycare.com')

WebUI.setText(findTestObject('359_OR/Page_IntelyCare Login/input_password'), 'skHealth1')

WebUI.click(findTestObject('359_OR/Page_IntelyCare Login/a_Login'))

WebUI.verifyElementPresent(findTestObject('359_OR/Page_IntelyCare Care Management/button_97.0'), 5)

WebUI.verifyElementPresent(findTestObject('359_OR/Page_IntelyCare Care Management/button_93.5'), 5)

WebUI.verifyElementPresent(findTestObject('359_OR/Page_IntelyCare Care Management/button_111'), 5)

WebUI.verifyElementPresent(findTestObject('359_OR/Page_IntelyCare Care Management/h3_FILL Rate'), 5)

WebUI.verifyElementPresent(findTestObject('359_OR/Page_IntelyCare Care Management/h3_LM Rate'), 5)

WebUI.verifyElementPresent(findTestObject('359_OR/Page_IntelyCare Care Management/h3_TODAY'), 5)

WebUI.verifyElementPresent(findTestObject('359_OR/Page_IntelyCare Care Management/th_Patient'), 5)

WebUI.verifyElementPresent(findTestObject('359_OR/Page_IntelyCare Care Management/th_Req ID'), 5)

WebUI.verifyElementPresent(findTestObject('359_OR/Page_IntelyCare Care Management/th_Date'), 5)

WebUI.verifyElementPresent(findTestObject('359_OR/Page_IntelyCare Care Management/th_City'), 5)

WebUI.verifyElementPresent(findTestObject('359_OR/Page_IntelyCare Care Management/th_State'), 5)

WebUI.verifyElementPresent(findTestObject('359_OR/Page_IntelyCare Care Management/th_Time'), 5)

WebUI.verifyElementPresent(findTestObject('359_OR/Page_IntelyCare Care Management/th_Hrs'), 5)

WebUI.verifyElementPresent(findTestObject('359_OR/Page_IntelyCare Care Management/th_Diff'), 5)

WebUI.verifyElementPresent(findTestObject('359_OR/Page_IntelyCare Care Management/th_Status'), 5)

WebUI.verifyElementPresent(findTestObject('359_OR/Page_IntelyCare Care Management/th_Type'), 5)

WebUI.verifyElementPresent(findTestObject('359_OR/Page_IntelyCare Care Management/th_Provider'), 5)

WebUI.verifyElementPresent(findTestObject('359_OR/Page_IntelyCare Care Management/th_Msg Status'), 5)

WebUI.verifyElementPresent(findTestObject('359_OR/Page_IntelyCare Care Management/th_top row'), 5)

WebUI.closeBrowser()

