import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://intelycare.com/hub')

WebUI.setText(findTestObject('360_OR/Page_IntelyCare Login/input_email'), 'skulkarni@intelycare.com')

WebUI.setText(findTestObject('360_OR/Page_IntelyCare Login/input_password'), 'skHealth1')

WebUI.click(findTestObject('360_OR/Page_IntelyCare Login/a_Login'))

WebUI.selectOptionByLabel(findTestObject('360_OR/Page_IntelyCare Care Management/select_view'), 'Today', false)



WebUI.selectOptionByLabel(findTestObject('360_OR/Page_IntelyCare Care Management/select_view'), 'Tomorrow', false)

WebUI.selectOptionByLabel(findTestObject('360_OR/Page_IntelyCare Care Management/select_view'), 'Current Week', false)

WebUI.closeBrowser()
