<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h3_FILL Rate</name>
   <tag></tag>
   <elementGuidId>1649d09c-c384-4607-91e6-d61a736be6e3</elementGuidId>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;goal&quot;)/fieldset[@class=&quot;ui-grid-a&quot;]/fieldset[@class=&quot;ui-grid-a&quot;]/div[@class=&quot;ui-block-b&quot;]/div[@class=&quot;ui-grid-b&quot;]/div[@class=&quot;ui-block-a&quot;]/div[1]/h3[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>FILL Rate</value>
   </webElementProperties>
</WebElementEntity>
